/**
 * 
 */

window.onload = initAll;
var usedNums = new Array(76);
var randomNums = new Array(76);
var actualTry;
var actualColorSetting = 1;


function getRandomNumber() {
	return Math.floor((Math.random() * 75) + 1);
}

function prepareViewOnStart() {
	document.getElementById("actualTryTitle").innerHTML = actualTry;
	document.getElementById("startButton").className = "gameButtonDisabled";
	document.getElementById("newCardButton").className = "gameButtonDisabled";
	document.getElementById("nextNumberButton").className = "gameButton";
}

function startGame() {
	randomNums = new Array(76);
	showNumber();
	actualTry = 1;
	prepareViewOnStart();
}

function updateScores(newNum) {
	document.getElementById("actualNumber").innerHTML = newNum;
	actualTry++;
	document.getElementById("actualTryTitle").innerHTML = actualTry;
}

function showNumber() {
	var newNum;
	do {
		newNum = getRandomNumber();
	} while (randomNums[newNum]);

	updateScores(newNum);
	
	randomNums[newNum] = true;
	if (usedNums[newNum] != null) {
		var currSquare = "square" + usedNums[newNum];
		document.getElementById(currSquare).className = 'fieldDrawn';
		checkWin();
	}
}

function clearScores() {
	document.getElementById("actualNumber").innerHTML = "&nbsp"
	document.getElementById("actualTryTitle").innerHTML = "&nbsp";
}

function assignFuncionalityToHtmlElements() {
	document.getElementById("newCardButton").onclick = anotherCard;
	document.getElementById("nextNumberButton").onclick = showNumber;
	document.getElementById("nextNumberButton").className = "gameButtonDisabled";
	document.getElementById("startButton").onclick = startGame;
}

function initAll() {
	if (document.getElementById) {
		assignFuncionalityToHtmlElements();
		newCard();
	} else {
		alert("Chujowa przeglądarka");
	}
}

function changeColors() {
	var i = actualColorSetting % 4 +1;
	document.getElementById("bingoCardId").className = "bingoCard" + i;
	document.getElementById("cardHeader").className = "cardHeadercolor" + i;
	actualColorSetting++;
}

function newCard() {
	changeColors();
	for (var i = 0; i < 24; i++) {
		setSquare(i);
	}
}

function setSquare(thisSquare) {
	var currSquare = "square" + thisSquare;
	var colPlace = new Array(0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3,
			3, 3, 4, 4, 4, 4, 4);
	var colBasis = colPlace[thisSquare] * 15;
	var newNum;

	do {
		newNum = colBasis + getNewNum() + 1;
	} while (usedNums[newNum] != null);

	usedNums[newNum] = thisSquare;
	var element = document.getElementById(currSquare);
	element.innerHTML = newNum;
	element.className = "field";

}

function getNewNum() {
	return Math.floor(Math.random() * 15);
}

function anotherCard() {
	for (var i = 1; i < usedNums.length; i++) {
		usedNums[i] = null;
		randomNums[i] = null;
	}

	newCard();
	return false;
}

function prepareViewAndFunctionallityForNewGame() {
	var element = document.getElementById("newCardButton");
	element.onclick = anotherCard;
	element.innerHTML = "New Card";
	element = document.getElementById("startButton");
	element.className = "gameButton";
	anotherCard();
}

function newGame() {
	clearScores();
	prepareViewAndFunctionallityForNewGame();
}

function endGame() {
	document.getElementById("nextNumberButton").className = "gameButtonDisabled";
	var element = document.getElementById("newCardButton");
	element.innerHTML = "New Game";
	element.onclick = newGame;
	element.className = "gameButton";
}

function checkWin() {
	var winningOption = -1;
	var setSquares = 0;
	var winners = new Array(31, 992, 15360, 507904, 541729, 557328, 1083458,
			2162820, 4329736, 8519745, 8659472, 16252928);

	for (var i = 0; i < 24; i++) {
		var currSquare = "square" + i;
		if (document.getElementById(currSquare).className == "fieldDrawn") {
			setSquares = (setSquares | (Math.pow(2, i)));
		}
	}

	for (var i = 0; i < winners.length; i++) {
		if ((winners[i] & setSquares) == winners[i]) {
			winningOption = i;
		}
	}

	if (winningOption > -1) {
		for (var i = 0; i < 24; i++) {
			if (winners[winningOption] & Math.pow(2, i)) {
				currSquare = "square" + i;
				document.getElementById(currSquare).className = "fieldWon";
			}
		}
		alert("Udało Ci sie po " + actualTry + " próbach");
		endGame();
		
	}
}
